import { SlashCommandBuilder } from "discord.js";
import * as sheets from "../util/sheets/index.js";
import * as steam from "../util/steam/index.js";

const isEmail = /^[^@]+@[^@]+\.[^@]{2,}$/u;

export const getInvite = {
  data: new SlashCommandBuilder()
    .setName("get-invite")
    .setDescription("Solicitar invite, requiere SteamID64")
    .addStringOption((option) =>
      option
        .setName("steamid64")
        .setDescription("SteamID64, usar www.steamidfinder.com para obtenerlo.")
        .setRequired(true)
    )
    .addStringOption((option) =>
      option
        .setName("email")
        .setDescription(
          "Email de la cuenta de steam, requerido para enviar el invite."
        )
        .setRequired(true)
    ),
  async execute(interaction) {
    await interaction.deferReply({ ephemeral: true });
    const steamidOption = interaction.options.getString("steamid64");
    const emailOption = interaction.options.getString("email").trim();
    const memberId = interaction.member.id;

    if (!isEmail.test(emailOption)) {
      return await interaction.editReply(
        `${emailOption} no parece ser un mail válido, tiene que ser el mail de la cuenta de steam para poder enviar el invite.`
      );
    }

    const steamID = await steam.resolveSteamID(steamidOption);
    if (steamID === null) {
      return await interaction.editReply(
        `${steamidOption} no parece ser un SteamID64 o una url de perfil valido.`
      );
    }

    const rows = await sheets.fetch();
    const alreadyExists = rows.some(
      (row) =>
        row[sheets.Columns.SteamID64] === steamID ||
        row[sheets.Columns.DiscordID] === memberId ||
        row[sheets.Columns.Email] === emailOption
    );
    if (alreadyExists) {
      return await interaction.editReply(
        "Solo un invite por user de steam / discord, si crees que esto es un error, comunicate con un administrador."
      );
    }
    /* eslint-disable no-undefined */
    const row = [
      emailOption,
      steamID,
      undefined, // Steam URL - Configured later by cron job
      undefined, // Steam Name - Configured later by cron job
      undefined, // Steam Friend? - Configured later by cron job
      undefined, // Steam Friend days - Configured later by cron job
      undefined, // Invite Enviado?
      interaction.member.user.username,
      interaction.member.displayName,
      memberId,
      false,
      false,
      "",
    ];
    /* eslint-enable no-undefined */

    await sheets.append([row]);
    return await interaction.editReply(
      "Steam ID almacenado correctamente!\nPor favor añade a esta cuenta a tu lista de amigos!\nhttps://steamcommunity.com/profiles/76561199701891787"
    );
  },
};
