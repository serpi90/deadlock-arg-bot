import { SlashCommandBuilder } from "discord.js";
import * as sheets from "../util/sheets/index.js";

export const status = {
  data: new SlashCommandBuilder()
    .setName("status")
    .setDescription("Ver estado de solicitud de invite"),
  async execute(interaction) {
    await interaction.deferReply({ ephemeral: true });
    const memberId = interaction.member.id;

    const rows = await sheets.fetch();
    const row = rows.find((r) => r[sheets.Columns.DiscordID] === memberId);

    const userName = row[sheets.Columns.steamUserName];
    const url = row[sheets.Columns.SteamURL];
    const id = row[sheets.Columns.SteamID64];
    const isFriend = row[sheets.Columns.SteamFriend];
    const days = row[sheets.Columns.SteamFriendDays];
    const inviteSent = row[sheets.Columns.InviteSent];
    const hasError = row[sheets.Columns.HasError];

    let response = `Steam: ${userName || url || "?"} [${id}]\n`;
    response += `Invitacion enviada: ${inviteSent ? "✅" : "❌"}\n`;
    if (!inviteSent) {
      response += `Amigo: ${isFriend ? `✅ (${days} días)` : "❌"}\n`;
      if (hasError) {
        response += `Hay un error con tu invite, comunicate con un admin.\n`;
      }
    }

    return await interaction.editReply(response);
  },
};
