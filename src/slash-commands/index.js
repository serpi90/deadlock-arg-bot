import { getInvite } from "./get-invite.js";
import { status } from "./status.js";

export const slashCommands = [getInvite, status];

export function forInteraction(interaction) {
  const commandName = interaction.commandName;
  return slashCommands.find((c) => c.data.name === commandName);
}
