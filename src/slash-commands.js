/* eslint-disable no-console */
import "dotenv/config";

import { Client, Events } from "discord.js";
import { forInteraction } from "./slash-commands/index.js";
import { syncCommands, token } from "./util/discord/index.js";

async function handleSlashCommand(interaction) {
  if (!interaction.isChatInputCommand()) return;

  const command = forInteraction(interaction);

  if (!command) return;

  try {
    await command.execute(interaction);
  } catch (error) {
    console.error(error);
    const content = "There was an error while executing this command!";
    if (interaction.replied || interaction.deferred) {
      await interaction.followUp({ content, ephemeral: true });
    } else {
      await interaction.reply({ content, ephemeral: true });
    }
  }
}

function startBot() {
  const client = new Client({ intents: [] });
  client.once(Events.ClientReady, (readyClient) => {
    console.log(`Client Ready! Logged in as ${readyClient.user.tag}`);
  });
  client.on(Events.InteractionCreate, handleSlashCommand);
  client.login(token);
}

await syncCommands();
startBot();
