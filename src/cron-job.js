/* eslint-disable no-await-in-loop */
import "dotenv/config";

import * as discord from "./util/discord/index.js";
import * as sheets from "./util/sheets/index.js";
import { Columns } from "./util/sheets/index.js";
import * as steam from "./util/steam/index.js";

const DeadlockAppID = 1422450;

function recordError(row, error) {
  row[Columns.HasError] = true;
  row[Columns.Comment] = `${row[Columns.Comment]} ${error}`;
}

const steamFriends = await steam.getFriendList(steam.invitesUserId);
const values = await sheets.fetch();

let updated = false;

for (const row of values) {
  const discordID = row[Columns.DiscordID];
  const inviteSent = row[Columns.InviteSent];
  row[Columns.Comment] ??= "";

  // eslint-disable-next-line no-continue
  if (row[Columns.HasError] || !discordID || !row[Columns.SteamID64]) continue;

  const steamID = await steam.resolveSteamID(row[Columns.SteamID64]);
  if (!steamID) {
    recordError(row, `Can't resolve SteamID64`);
    updated = true;
  } else if (steamID !== row[Columns.SteamID64]) {
    row[Columns.SteamID64] = steamID;
    updated = true;
  }

  const fetchSteamInfo = !row[Columns.SteamURL] || !row[Columns.SteamName];
  if (fetchSteamInfo) {
    try {
      const { profileurl, personaname } = await steam.getPlayerSummary(steamID);
      row[Columns.SteamURL] = profileurl;
      row[Columns.SteamName] = personaname;

      const games = await steam.getOwnedGames(steamID);
      if (games.some((game) => game.appid === DeadlockAppID)) {
        row[Columns.InviteSent] = true;
      }
    } catch (error) {
      recordError(row, `Can't resolve Steam Name: [${error.message}]`);
    }
    updated = true;
  }

  const friend = steamFriends.find(({ steamid }) => steamid === steamID);
  row[Columns.SteamFriend] = Boolean(friend);
  row[Columns.SteamFriendDays] = friend?.friend_days;

  const fetchDiscordInfo = row[Columns.DiscordUser];
  if (!fetchDiscordInfo) {
    try {
      const data = await discord.getGuildMember(discordID);
      row[Columns.DiscordUser] = data.user.username;
      row[Columns.DiscordName] = data.nick ?? data.user.global_name;
      row[Columns.DiscordRoleSet] = data.roles.includes(discord.roleId);
    } catch (error) {
      recordError(row, `Can't resolve Discord User: [${error.message}]`);
    }
    updated = true;
  }

  const addVerifiedrole = inviteSent && !row[Columns.DiscordRoleSet];
  if (addVerifiedrole) {
    try {
      await discord.addGuildMemberRole(discordID);
      row[Columns.DiscordRoleSet] = true;
    } catch (error) {
      recordError(row, `Can't set Discord Role: [${error.message}]`);
    }
    updated = true;
  }

  if (!inviteSent && !row[Columns.Email]) {
    recordError(row, `Missing email`);
    updated = true;
  }
}

if (updated) {
  await sheets.update(values);
}
