/** Google Sheets */
import google from "@googleapis/sheets";

const auth = new google.auth.GoogleAuth({
  keyFile: "./google-service-account-credentials.json",
  scopes: ["https://www.googleapis.com/auth/spreadsheets"],
});

export const sheets = google.sheets({
  version: "v4",
  auth: await auth.getClient(),
});
