import { sheets } from "./client.js";
import { range, spreadsheetId } from "./constants.js";

export async function fetch() {
  const response = await sheets.spreadsheets.values.get({
    spreadsheetId,
    range,
    valueRenderOption: "UNFORMATTED_VALUE",
  });
  return response.data.values;
}
