import { sheets } from "./client.js";
import { range, spreadsheetId } from "./constants.js";

export async function append(values) {
  return await sheets.spreadsheets.values.append({
    spreadsheetId,
    range,
    insertDataOption: "INSERT_ROWS",
    resource: { values },
    valueInputOption: "RAW",
  });
}
