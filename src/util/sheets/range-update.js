import { sheets } from "./client.js";
import { range, spreadsheetId } from "./constants.js";

export async function update(values) {
  return await sheets.spreadsheets.values.update({
    spreadsheetId,
    range,
    resource: { values },
    valueInputOption: "RAW",
  });
}
