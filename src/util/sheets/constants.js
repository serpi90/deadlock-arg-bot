export const spreadsheetId = process.env.GOOGLE_SHEET_ID;
export const range = process.env.GOOGLE_SHEET_RANGE;
