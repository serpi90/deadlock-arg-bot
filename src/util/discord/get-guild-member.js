import { Routes } from "discord.js";
import { rest } from "./client.js";
import { guildId } from "./constants.js";

/* {
  "avatar": null,
  "communication_disabled_until": null,
  "flags": 106,
  "joined_at": "2024-06-11T14:43:36.033000+00:00",
  "nick": null,
  "pending": false,
  "premium_since": null,
  "roles": [
    "1245767024139964610"
  ],
  "unusual_dm_activity_until": null,
  "user": {
    "id": "319923627283841026",
    "username": "serpi90",
    "avatar": "6ca208ef233237d73307f945f203bc21",
    "discriminator": "0",
    "public_flags": 0,
    "flags": 0,
    "banner": null,
    "accent_color": 0,
    "global_name": "Serpi90",
    "avatar_decoration_data": null,
    "banner_color": "#000000",
    "clan": null
  },
  "mute": false,
  "deaf": false
} */
export async function getGuildMember(userId) {
  return await rest.get(Routes.guildMember(guildId, userId));
}
