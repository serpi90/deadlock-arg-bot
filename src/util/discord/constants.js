export const guildId = process.env.DISCORD_GUILD_ID;
export const roleId = process.env.DISCORD_ROLE_ID;
export const token = process.env.DISCORD_AUTH_TOKEN;
export const appId = process.env.DISCORD_APP_ID;
