import { Routes } from "discord.js";
import { rest } from "./client.js";
import { guildId, roleId } from "./constants.js";

export async function addGuildMemberRole(memberId) {
  const route = Routes.guildMemberRole(guildId, memberId, roleId);
  return await rest.put(route);
}
