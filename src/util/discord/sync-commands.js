import { REST, Routes } from "discord.js";
import { slashCommands } from "../../slash-commands/index.js";
import { appId, token } from "./constants.js";

export async function syncCommands() {
  const rest = new REST().setToken(token);
  const body = slashCommands.map((command) => command.data.toJSON());
  const route = Routes.applicationCommands(appId);

  const data = await rest.put(route, { body });
  // eslint-disable-next-line no-console
  console.log(`Synced ${data.length} application (/) commands.`);
}
