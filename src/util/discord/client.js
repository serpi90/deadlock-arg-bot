import { REST } from "discord.js";
import { token } from "./constants.js";

export const rest = new REST().setToken(token);
