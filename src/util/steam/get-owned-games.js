import { apiKey } from "./constants.js";

/* {
  "response": {
    "game_count": 596,
    "games": [
      {
        "appid": 3830,
        "playtime_forever": 936
      },
      {
        "appid": 4000,
        "playtime_forever": 0
      }
    ]
  }
} */

export async function getOwnedGames(steamId) {
  const api = new URL(
    "http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/"
  );
  api.searchParams.append("key", apiKey);
  api.searchParams.append("steamid", steamId);
  api.searchParams.append("format", "json");
  const response = await fetch(api);
  const body = await response.json();

  return body.response.games ?? []; // Private profiles don't return the games field
}
