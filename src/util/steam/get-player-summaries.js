import { apiKey } from "./constants.js";
/* {
  "response": {
    "players": [
      {
        "steamid": "76561199701891787",
        "communityvisibilitystate": 3,
        "profilestate": 1,
        "personaname": "Deadlock Argentina",
        "profileurl": "https://steamcommunity.com/profiles/76561199701891787/",
        "avatar": "https://avatars.steamstatic.com/e4841298596909a2454c3d36b623ee6d39fa97fd.jpg",
        "avatarmedium": "https://avatars.steamstatic.com/e4841298596909a2454c3d36b623ee6d39fa97fd_medium.jpg",
        "avatarfull": "https://avatars.steamstatic.com/e4841298596909a2454c3d36b623ee6d39fa97fd_full.jpg",
        "avatarhash": "e4841298596909a2454c3d36b623ee6d39fa97fd",
        "personastate": 0,
        "primaryclanid": "103582791429521408",
        "timecreated": 1718116047,
        "personastateflags": 0,
        "loccountrycode": "AR"
      }
    ]
  }
} */
export async function getPlayerSummaries(steamids) {
  const api = new URL(
    `http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/`,
  );
  api.searchParams.append("key", apiKey);
  api.searchParams.append("steamids", steamids.join(","));

  const response = await fetch(api);
  const body = await response.json();
  return body.response.players.map((_) => ({
    steamid: _.steamid,
    profileurl: _.profileurl,
    personaname: _.personaname,
  }));
}

export async function getPlayerSummary(steamid) {
  const summaries = await getPlayerSummaries([steamid]);
  return summaries[0];
}
