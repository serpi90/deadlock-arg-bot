/* eslint-disable require-unicode-regexp */
import { apiKey } from "./constants.js";

const isSteamID64 = /^7[0-9]{16}$/;
const isProfileURL =
  /^https?:\/\/steamcommunity.com\/profiles\/(?<steamid>[0-9]*)\/?/;
const isCustomURL = /^https?:\/\/steamcommunity.com\/id\/(?<user>[^/]*)\/?/;

async function resolveIdFromVanityURL(username) {
  try {
    const api = new URL(
      "https://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/",
    );
    api.searchParams.append("key", apiKey);
    api.searchParams.append("vanityurl", username);

    const response = await fetch(api);
    const body = await response.json();
    return body?.response.success ? body.response.steamid : null;
    // eslint-disable-next-line no-unused-vars
  } catch (error) {
    return null;
  }
}

export async function resolveSteamID(idOrURL) {
  const str = `${idOrURL}`.trim();
  if (isSteamID64.test(str)) return str;

  let match = str.match(isProfileURL);
  if (match) return match.groups.steamid;

  match = str.match(isCustomURL);
  if (match) return await resolveIdFromVanityURL(match.groups.user);

  return null;
}
