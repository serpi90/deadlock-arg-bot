import { apiKey } from "./constants.js";

/* {
  "friendslist": {
    "friends": [
      {
        "steamid": "76561198050336522",
        "relationship": "friend",
        "friend_since": 1718116472
      },
      {
        "steamid": "76561198105399641",
        "relationship": "friend",
        "friend_since": 1718118132
      }
    ]
  }
} */
export async function getFriendList(steamId) {
  const api = new URL(
    "https://api.steampowered.com/ISteamUser/GetFriendList/v0001/",
  );
  api.searchParams.append("key", apiKey);
  api.searchParams.append("steamid", steamId);
  api.searchParams.append("relationship", "friend");
  const response = await fetch(api);
  const body = await response.json();

  const now = new Date().getTime() / 1000;
  const friends = body.friendslist.friends;
  for (const friend of friends) {
    // eslint-disable-next-line camelcase
    friend.friend_days = Math.trunc((now - friend.friend_since) / 86400);
  }
  return friends;
}
