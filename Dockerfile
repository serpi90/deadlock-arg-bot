FROM node:22-alpine

WORKDIR /app

COPY package.json package-lock.json /app/
RUN npm ci --omit=dev

COPY src/ /app/src/

STOPSIGNAL SIGTERM

CMD [ "node", "src/slash-commands.js" ]
